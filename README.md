LZMA
====

A managed LZMA implementation written from scratch in C#.

Original LZMA implementation and reference source code by Igor Pavlov: http://www.7-zip.org/sdk.html

The source code aims to provide a clean, safe and well-documented implementation of the LZMA compression encoder and decoder as well as some reusable classes to develop custom LZ-based compression algorithms.


Encoder
-------

The encoder is functional, but is currently rather simple in terms of algorithm complexity. It does not do much compression optimizations (and speed optimizations as well).
Points of interest are:
 - Better match finding algorithms.
 - "Optimal" encoding (like the XZ and 7zip normal encoder does).
 - Multiple encoding levels (e.g. fast, normal, best).


Decoder
-------

The decoder is feature complete and should be able to decode all standard LZMA streams. The performance is pretty good as well.


Formats
-------

The only supported LZMA format is the "standard" LZMA file format with a 13-byte header (1 byte properties, 4 byte dictionary size and 8 byte original length of source data).
Points of interest are:
 - An LZMA2 encoder (that actually makes use of the "uncompressed" chunk feature) and decoder.
 - Maybe support for XZ streams (might be a bit out of scope).


Compiling
---------

The source code can be compiled with Visual Studio and Mono (e.g. xbuild).


nitroxis, 2014
